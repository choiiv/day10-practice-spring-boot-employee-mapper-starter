package com.afs.restapi.entity;

import com.afs.restapi.mapper.EmployeeMapper;

import java.util.ArrayList;
import java.util.List;

public class CompanyResponse {
    private Long id;
    private String name;
    private List<EmployeeResponse> employees;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EmployeeResponse> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = new ArrayList<>();
        EmployeeMapper employeeMapper = new EmployeeMapper();
        employees.forEach(employee -> this.employees.add(employeeMapper.toResponse(employee)));
    }
}
