package com.afs.restapi.entity;

public class EmployeeRequest {

    private String name;
    private Integer age;
    private String gender;
    private Integer salary;
    private Long companyId;

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public Integer getSalary() {
        return salary;
    }

    public Long getCompanyId() {
        return companyId;
    }
}
